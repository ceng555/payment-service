package tr.edu.iyte.ceng555.paymentservice.controller;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import tr.edu.iyte.ceng555.paymentservice.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    private final PaymentService paymentService;
    private final KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    public PaymentController(PaymentService paymentService, KafkaTemplate<String, String> kafkaTemplate) {
        this.paymentService = paymentService;
        this.kafkaTemplate = kafkaTemplate;
    }

    @PostMapping("/kafka/test")
    public void kafkaTest() {
        kafkaTemplate.send("test1", "deneme");
    }

    @KafkaListener(topics = "test1", groupId = "groupId")
    public void listenGroupFoo(String message) {
        System.out.println("Received Message in group foo: " + message);
    }

}
