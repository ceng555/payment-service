package tr.edu.iyte.ceng555.paymentservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import tr.edu.iyte.ceng555.paymentservice.model.FishRecord;

@Service
public class PaymentService {

    private final Logger logger = LoggerFactory.getLogger(PaymentService.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final Double COOPERATIVE_COMMISSION = 0.10;
    private final Double CENG555_SOFTWARE_COMMISSION = 0.03;

    @Autowired
    public PaymentService(KafkaTemplate<String, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @KafkaListener(topics = "record-finished", groupId = "groupId")
    public void consumeRecordFinished(String message) throws JsonProcessingException {
        logger.info("Topic: record-finished, Message: {}", message);
        FishRecord fishRecord = objectMapper.readValue(message, FishRecord.class);

        double cooperativeRevenue = fishRecord.getPrice() * COOPERATIVE_COMMISSION;
        double ceng555Revenue = fishRecord.getPrice() * CENG555_SOFTWARE_COMMISSION;
        double fishermanRevenue = fishRecord.getPrice() - (cooperativeRevenue + ceng555Revenue);

        transferRevenue("COOPERATIVE", cooperativeRevenue);
        transferRevenue("CENG555", ceng555Revenue);
        transferRevenue("FISHERMAN", fishermanRevenue);

        String invoice = generateInvoice(cooperativeRevenue, ceng555Revenue, fishermanRevenue, fishRecord.getPrice());
        kafkaTemplate.send("invoice-created", invoice);
    }


    private void transferRevenue(String stakeholder, double amount) {
        logger.info("transfer revenue, stakeholder: {}, amount: {}", stakeholder, amount);
    }

    private String generateInvoice(double cooperativeRevenue, double ceng555Revenue, double fishermanRevenue, double total) {
        return "=== Invoice ===\n" +
                "cooperativeRevenue: " + cooperativeRevenue + "\n" +
                "ceng555Revenue: " + ceng555Revenue + "\n" +
                "fishermanRevenue: " + fishermanRevenue + "\n\n" +
                "total: " + total + "\n";
    }

}
